export const collection = (props) => {
  return `<div>
    <div class="name">${props.title}</div>
    ${props.children}
</div>`;
};

import { header, collection } from "../components/index.js";
import { currentURL } from "../utils/index.js";

const stat = {
  title: "Stats",
  children: header(),
};

const game = {
  title: "Games",
  children: `<a href="${currentURL()}?page=games">Next Page</a>`,
};

const homepage = () => {
  return `${header()}
  ${collection(stat)}
  ${collection(game)}
  `;
};

export default homepage;

import home from "./src/pages/home.js";
import games from "./src/pages/game.js";
import greenlight from "./src/pages/greenlight.js";

var renderRoot = (element) => {
  const root = document.getElementById("root");
  root.innerHTML = element;
};

const initialLoad = () => {
  const url = new URL(window.location);
  const page = url.searchParams.get("page");
  router(page);
};

const router = (page) => {
  switch (page) {
    case "home":
      renderRoot(home());
      break;
    case "games":
      renderRoot(games());
      break;
    case "greenlight":
      renderRoot(greenlight());
      break;
    default:
      renderRoot(home());
      break;
  }
};

window.addEventListener("DOMContentLoaded", () => {
  initialLoad();

  window.navigation.addEventListener("navigate", (event) => {
    const url = new URL(event.destination.url);
    const page = url.searchParams.get("page");
    router(page);
  });
});
